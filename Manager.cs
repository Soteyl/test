﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Manager : MonoBehaviour
{
    public int score,step = 1, CoroutineID, BoosterID;
    public GameObject ShopScr;
    public Text ScoreText;
    public GameObject[] items;
    Save save = new Save(); 
    bool temp_active;

    public void OnClcShop()
    {
        ShopScr.SetActive(!ShopScr.activeSelf);
    }
//
    public void OnItemClick(int id)
    {
        item = items[id].GetComponentInChildren<Item>();

        switch (item.type)
        {
            case 1:
                if (score >= item.price)
                {
                    step += item.bonus;
                    score -= item.price;
                    items[id].item.price *= 2;
                }
                break;

            case 2:
                CoroutineID = id;
                if (score >= item.price)
                {
                    item.bonus ++;
                    score -= item.price;
                    item.workers ++;
                    item.price *= 2;
                    
                }
                break;

            case 3:
                BoosterID = id;
                if (score >= item.price)
                {
                    score -= item.price;
                    StartCoroutine(Boooster(id));
                }
                break;

        }

    }

    IEnumerator BonusPerSec(int id)
    {
        items = items[id].GetComponentInChildren<Item>();
        while (true)
        {
            score += item.bonus;
            yield return new WaitForSeconds(item.time);
        }
    }

    IEnumerator Boooster(int id)
    {
        items[id].SetActive(false);
        items[CoroutineID].GetComponentInChildren<Item>().bonus *= 10;
        yield return new WaitForSeconds(items[id].GetComponentInChildren<Item>().time);
        items[CoroutineID].GetComponentInChildren<Item>().bonus /= 10;
        items[id].SetActive(true);

    }
 
    public void OnClick()
    {
        score += step;
    }


    void Start()
    {
        StartCoroutine(BonusPerSec(CoroutineID)); /// При старте курутин ид = 0 - лишнее
    }

    void Update()
    {
        ScoreText.text = "$" + score;

        if (items[CoroutineID].GetComponentInChildren<Item>().workers <= 0)
        {
            items[BoosterID].SetActive(false);//Отключение кнопки бустера при отсутствии рабочих
        }
        else
        {
            items[BoosterID].GetComponentInChildren<Item>().price = items[BoosterID].GetComponentInChildren<Item>().starting_price_NOTusetosetprice * items[CoroutineID].GetComponentInChildren<Item>().workers;//Оновление цены бустера
            //Можно было просто присвоить значение Bonus из Рабочих -_-
            if (temp_active != true)
            {
                temp_active = true;
                items[BoosterID].SetActive(true);
            }
        }

        for (int id = 0; id < items.Length; id++) // Обновление цен(текст)
        {
            items[id].GetComponentInChildren<Text>().text = "" + items[id].GetComponentInChildren<Item>().txt + "\nЦена: " + items[id].GetComponentInChildren<Item>().price + "$";
        }

    }

    private void Awake()
    {
        if (PlayerPrefs.HasKey("SAVE"))
        {
            save = JsonUtility.FromJson<Save>(PlayerPrefs.GetString("SAVE"));
            score = save.score;
            items[CoroutineID].GetComponentInChildren<Item>().workers = save.workers;
            items[CoroutineID].GetComponentInChildren<Item>().bonus = save.bonus;
            for (int i = 0; i < items.Length; i++)
            {
                items[i].GetComponentInChildren<Item>().price = save.price[i];
            }

        }
    }
#if UNITY_ANDROID && !UNITY_EDITOR
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            save.score = score;
            if (items[CoroutineID].GetComponentInChildren<Item>().workers < items[CoroutineID].GetComponentInChildren<Item>().bonus) items[CoroutineID].GetComponentInChildren<Item>().bonus /= 10; //Фикс бага
            save.bonus = items[CoroutineID].GetComponentInChildren<Item>().bonus;
            save.workers = items[CoroutineID].GetComponentInChildren<Item>().workers;
            save.price = new int[items.Length];
            for (int i = 0; i < items.Length; i++)
            {
                save.price[i] = items[i].GetComponentInChildren<Item>().price;
            }
            PlayerPrefs.SetString("SAVE", JsonUtility.ToJson(save));
        }
    }
#else

    private void OnApplicationQuit()
    {
        save.score = score;
        if(items[CoroutineID].GetComponentInChildren<Item>().workers < items[CoroutineID].GetComponentInChildren<Item>().bonus) items[CoroutineID].GetComponentInChildren<Item>().bonus /= 10; //Фикс бага
        save.bonus = items[CoroutineID].GetComponentInChildren<Item>().bonus;
        save.workers = items[CoroutineID].GetComponentInChildren<Item>().workers;
        save.price = new int[items.Length];
        for (int i = 0; i < items.Length; i++)
        {
            save.price[i] = items[i].GetComponentInChildren<Item>().price;
        }
        PlayerPrefs.SetString("SAVE", JsonUtility.ToJson(save));
    }
#endif

}

[System.Serializable]
public class Save
{
    public int score;
    public int[] price;
    public int workers;
    public int bonus;
}
